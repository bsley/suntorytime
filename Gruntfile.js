module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        stylus: {
            options: {
                'compress': false,
                'include css': true
            },
            compile: {
                files: {
                  'html/master.css': 'app/_styles/master.styl',
                }
            }
        },
        concat: {   
            dist: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    // 'bower_components/headroom.js/dist/headroom.js',
                    // 'bower_components/headroom.js/dist/jQuery.headroom.js',
                    // 'bower_components/slick.js/slick/slick.js',
                    // 'bower_components/hammerjs/hammer.js',
                    // 'bower_components/jquery.hammer.js/jquery.hammer.js',
                    // 'bower_components/pace/pace.js',
                    // 'bower_components/velocity/velocity.js',
                    // 'bower_components/velocity/velocity.ui.js',
                    // 'bower_components/bower-skrollr/skrollr.min.js',
                    // 'bower_components/deep-link.js/build/deep-link.min.js',
                    'bower_components/canham.typed.js/dist/typed.min.js',

                    'app/_scripts/main.js'
                ], 
                dest: 'html/master.js',
            }
        },
        watch: {
            scripts: {
                files: ['app/_scripts/*.js', 'Gruntfile.js', 'app/_styles/*.styl'],
                tasks: ['concat', 'stylus'],
                options: {
                    spawn: false
                },
            } 
        },
        // cssmin: {
        //     options: {
        //         shorthandCompacting: false,
        //         roundingPrecision: -1
        //     },
        //     target: {
        //         files: {
        //             'html/master_min.css': ['html/master.css']
        //         }
        //     }
        // },
        // browserSync: {
        //     dev: {
        //         bsFiles: {
        //             src : 'html/_css/*.css'                },
        //         options: {
        //             watchTask: true,
        //             proxy: 'http://localhost:8888/suntorytime/html',
        //             startPath: 'suntorytime/html/',
        //         }
        //     }
        // }
        

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'stylus', 'watch' ]);

};